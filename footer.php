<?php
/**
 * The template for displaying the footer.
 * Calls sidebar-footer.php for bottom widgets.
 */
?>
<div id="footer" class="clear span-10 last" role="contentinfo">

<?php
    /* A sidebar in the footer? Yep. You can can customize
     * your footer with four columns of widgets.
     */
    // get_sidebar( 'footer' );
?>

        <p class="small-text copyright">&copy; Copyright
        <?php
			$thisYear = getdate();
			if ($thisYear[year] >'2010') {
			  echo "2010 - $thisYear[year] ";
			} else {
			  echo "$thisYear[year] ";
			}
		?>
		  Electromusic.ro. <a href="http://www.electromusic.ro/politica-de-confidentialitate/">Confidențialitate</a></p>
    </div>
</div>

<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?><script>$('#commentform .twitlink').insertBefore('#commentform .form-notes-after').wrap('<p />');$('#commentform #comment_mail_notify').parent().remove().insertAfter('#commentform .form-notes-after').css({'overflow': 'hidden', 'border-bottom': 'none'});$('#comment_mail_notify').css({'width': '90px', 'float': 'left'});$('label[for="comment_mail_notify"]').html('Anunță-mă pe email dacă apar comentarii noi').css('width', '350px')</script>
</body>
</html>
