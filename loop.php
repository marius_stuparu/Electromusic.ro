<?php
/**
 * The loop that displays posts.
 *
 * The loop displays the posts and the post content.  See
 * http://codex.wordpress.org/The_Loop to understand it and
 * http://codex.wordpress.org/Template_Tags to understand
 * the tags used in it.
 */
?>

<!--
<div id="nav-above" class="clear span-3 push-2 prepend-top last">
	<div class="nav-previous"><?php next_posts_link( __( '<span></span>Mai vechi', 'electromusic' ) ); ?></div>
	<div class="nav-next"><?php previous_posts_link( __( 'Mai recente <span></span>', 'electromusic' ) ); ?></div>
</div>
-->

<?php /* If there are no posts to display, such as an empty archive page */ ?>
<?php if ( ! have_posts() ) : ?>
	<div id="post-0" class="post error404 not-found">
		<h1 class="entry-title"><?php _e( 'Nu am găsit', 'electromusic' ); ?></h1>
		<div class="entry-content">
			<p><?php _e( 'Îmi pare rău, articolul căutat nu există sau a fost mutat. Încearcă să-l cauți cu modulul SEARCH.', 'electromusic' ); ?></p>
			<?php get_search_form(); ?>
		</div>
	</div>
<?php endif; ?>

<?php // Start the Loop. ?>
<?php while ( have_posts() ) : the_post(); ?>
    <div id="post-<?php the_ID(); ?>" class="span-7 article-container prepend-top">
        <div class="article-head">
                <a href="<?php the_permalink(); ?>" class="article-permalink-button" title="<?php printf( esc_attr__( 'Permalink la %s', 'electromusic' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark">permalink</a>
                <h3 class="article-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink la %s', 'electromusic' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
                <h5 class="small-text article-date"><?php electromusic_posted_on(); ?> <?php edit_post_link( __( 'Edit', 'electromusic' ), '<span class="edit-link">&nbsp;|&nbsp;', '</span>' ); ?></h5>
                <h5 class="small-text comments-count"><?php comments_popup_link( __( 'Comentează', 'electromusic' ), __( '1 comentariu', 'electromusic' ), __( '% comentarii', 'electromusic' ) ); ?><span></span></h5>
        </div>

            <?php the_content( __( 'Citește continuarea', 'electromusic' ) ); ?>
            <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pagini:', 'electromusic' ), 'after' => '</div>' ) ); ?>

            <?php
                    $tags_list = get_the_tag_list( '', ', #' );
                    if ( $tags_list ): ?>
                    <div class="metadata">
                    <?php printf( __( 'Tags #%1$s', 'electromusic' ), $tags_list ); ?>
                    </div>
                <?php endif; ?>
            <div class="article-footer">
                <?php if ( count( get_the_category() ) ) : ?>
                <div class="category"><?php printf( __( '%1$s<span></span>', 'electromusic' ), get_the_category_list( ', ' ) ); ?></div>
                <?php endif; ?>
            </div>
        </div>

        <?php comments_template( '', true ); ?>

<?php endwhile; // End the loop. ?>

<div id="nav-below" class="clear span-3 push-2 prepend-top last">
	<div class="nav-previous"><?php next_posts_link( __( '<span></span>Mai vechi', 'electromusic' ) ); ?></div>
	<div class="nav-next"><?php previous_posts_link( __( 'Mai recente <span></span>', 'electromusic' ) ); ?></div>
</div>