<?php get_header(); ?>

<div id="content" class="clear span-7" role="main">

<?php /* If there are no posts to display, such as an empty archive page */ ?>
<?php if ( ! have_posts() ) : ?>
	<div id="post-0" class="post error404 not-found">
		<h1 class="entry-title"><?php _e( 'Nu am găsit', 'electromusic' ); ?></h1>
		<div class="entry-content">
			<p><?php _e( 'Îmi pare rău, articolul căutat nu există sau a fost mutat. Încearcă să-l cauți cu modulul SEARCH.', 'electromusic' ); ?></p>
			<?php get_search_form(); ?>
		</div>
	</div>
<?php endif; ?>

<?php if ( have_posts() ) : the_post(); ?>

<div id="post-<?php the_ID(); ?>" class="span-7 article-container">
        <div class="article-head">
                <a href="<?php the_permalink(); ?>" class="article-permalink-button" title="<?php printf( esc_attr__( 'Permalink la %s', 'electromusic' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark">permalink</a>
                <h3 class="article-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink la %s', 'electromusic' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
		  		<h5 class="small-text article-date"><?php electromusic_posted_on(); ?> <?php edit_post_link( __( 'Edit', 'electromusic' ), '<span class="edit-link">&nbsp;|&nbsp;', '</span>' ); ?></h5>
        </div>

            <?php the_content(); ?>

            <div class="article-footer"></div>
        </div>

<?php comments_template( '', true ); ?>

<?php endif; ?>

</div>

<div id="sidebar" class="span-3 last">
<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>
