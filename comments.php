<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains both current comments
 * and the comment form.  The actual display of comments is
 * handled by a callback to electromusic_comment which is
 * located in the functions.php file.
 */
?>

<div id="comments" class="span-7 article-container">
<?php if ( have_comments() ) : ?>
    <div class="article-head">
        <h3 class="comment-title"><?php
            printf( _n( 'Un comentariu la', '%1$s comentarii la', get_comments_number(), 'electromusic' ),
            get_comments_number() ); ?></h3>
        <h3 class="comment-title"><?php
            printf( _n( '%1$s', get_the_title(), 'electromusic' ),
            '<em>' . get_the_title() . '</em>' ); ?></h3>
    </div>
        <ol class="commentlist">
        <?php
            /* Loop through and list the comments. Tell wp_list_comments()
             * to use electromusic_comment() to format the comments.
             * If you want to overload this in a child theme then you can
             * define electromusic_comment() and that will be used instead.
             * See electromusic_comment() in electromusic/functions.php for more.
             */
            wp_list_comments( array( 'callback' => 'electromusic_comment' ) );
        ?>
        </ol>
    <div class="article-footer"></div>

    <?php endif; // end have_comments() ?>

    <?php
    $fields =  array(
    'author' => '<p class="comment-form-author">' . '<label for="author">' . __( 'Nume*', 'electromusic' ) . '</label> ' .
                '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="65"' . $aria_req . ' /></p>',
    'email'  => '<p class="comment-form-email"><label for="email">' . __( 'Email*', 'electromusic' ) . '</label> ' .
                '<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="65"' . $aria_req . ' /></p>',
    'url'    => '<p class="comment-form-url"><label for="url">' . __( 'Website', 'electromusic' ) . '</label>' .
                '<input id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="65" /></p>',
    );
    $defaults = array(
    'fields'               => apply_filters( 'comment_form_default_fields', $fields ),
    'comment_field'        => '<p class="comment-form-comment"><label for="comment">' . _x( 'Comentariu', 'noun', 'electromusic' ) . '</label><textarea id="comment" name="comment" cols="65" rows="5" aria-required="true"></textarea></p>',
    'must_log_in'          => '<p class="must-log-in">' .  sprintf( __( 'Trebuie să fii <a href="%s">logat</a> pentru a comenta.' ), wp_login_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>',
    'logged_in_as'         => '<p class="logged-in-as">' . sprintf( __( 'Logat ca <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out">Log out?</a>' ), admin_url( 'profile.php' ), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>',
    'comment_notes_before' => '<div class="comment-notes">' . __( 'Adresa de email nu va fi făcută publică. Câmpurile marcate cu * sunt obligatorii', 'electromusic' ) . '</div>',
    'comment_notes_after'  => '<div class="form-notes-after"></div>',
    'id_form'              => 'commentform',
    'id_submit'            => 'submit',
    'title_reply'          => __( 'Răspunde', 'electromusic' ),
    'title_reply_to'       => __( 'Răspunde lui %s', 'electromusic' ),
    'cancel_reply_link'    => __( 'Anulează', 'electromusic' ),
    'label_submit'         => __( 'Comentează', 'electromusic' ),
    );

    comment_form($defaults); ?>
</div>